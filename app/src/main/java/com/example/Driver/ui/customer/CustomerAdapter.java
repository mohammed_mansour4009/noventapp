package com.example.Driver.ui.customer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Driver.R;
import com.example.Driver.Util.ResourcesUtil;
import com.example.Driver.model.OrderInfoModel;

import java.util.List;


public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.viewHolder> {

    List<OrderInfoModel> orderInfoList;
    Context context;
    private int heightScreenRV;
 
    public CustomerAdapter(List<OrderInfoModel> orderInfoModels) {
        this.orderInfoList = orderInfoModels; //
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        context = recyclerView.getContext();
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public CustomerAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {//inflate of layout and Components
    
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_accommodation_infor, null, false);
        heightScreenRV = parent.getMeasuredHeight();
        
        CustomerAdapter.viewHolder viewHolder = new CustomerAdapter.viewHolder(v);//inflate of Components
        return viewHolder;
    }

    @Override
    public void onBindViewHolder( CustomerAdapter.viewHolder holder, int position) {// put new data ever time



        holder.tvUp.setText(orderInfoList.get(position).getTxUp());
        holder.tvDown.setText(orderInfoList.get(position).getTxDown());

        holder.ic.setImageResource(ResourcesUtil.getDrawableByName(
                orderInfoList.get(position).getIc(), context));
    }




    @Override
    public int getItemCount() {
        return orderInfoList.size();// here the issue
    }

    class viewHolder extends RecyclerView.ViewHolder {//declare elements and but resources
        ConstraintLayout constraintLayout;
        AppCompatImageView iconImageView;
        ImageView ic;
        TextView tvDown;//fix name please tvNameCustomer
        TextView tvUp;


        public viewHolder(@NonNull View itemView) {
            super(itemView);
            constraintLayout = itemView.findViewById(R.id.cl_info_user);
            ic = itemView.findViewById(R.id.ic_city_info);
            tvDown = itemView.findViewById(R.id.tv_down);
            tvUp = itemView.findViewById(R.id.tv_up);
        }


    }
}

