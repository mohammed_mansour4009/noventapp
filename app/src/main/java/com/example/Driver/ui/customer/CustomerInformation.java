package com.example.Driver.ui.customer;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Driver.R;
import com.example.Driver.Util.GsonUtil;
import com.example.Driver.Util.ReaderUtil;
import com.example.Driver.model.OrderInfoModel;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class CustomerInformation extends AppCompatActivity {
    RecyclerView rcOrder;
    List<OrderInfoModel> orderInfoModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_information);
        init();
        setUpRecyclerView();


    }

    private void init() {
        rcOrder = findViewById(R.id.re_order);
    }

          private void setUpRecyclerView() {

            rcOrder.setLayoutManager(new GridLayoutManager(this, 2));

            rcOrder.setAdapter(new CustomerAdapter(getInfoList()));//Connect  Adapter with RecyclerView

    }

    private List<OrderInfoModel> getInfoList() {

        return GsonUtil.getGson().fromJson(ReaderUtil.getResponse(R.raw.user_information, this)
                , new TypeToken<List<OrderInfoModel>>() {
                }.getType());
    }

}