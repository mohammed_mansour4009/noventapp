package com.example.Driver.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Driver.R;
import com.example.Driver.Util.GsonUtil;
import com.example.Driver.Util.ReaderUtil;
import com.example.Driver.model.CustomerModel;
import com.example.Driver.ui.customer.CustomerInformation;
import com.example.Driver.ui.user.UserLocationActivity;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {
    private RecyclerView rvMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);

        init();
        setUpRecyclerView();

    }


    public void init() {
        rvMain = findViewById(R.id.rv_main);
    }

    private void setUpRecyclerView() {
        rvMain.setLayoutManager( new LinearLayoutManager(this));
        rvMain.setAdapter( new DriverAdapter(getDriverList()));//Connect  Adapter with RecyclerView
    }


    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.popup_menu);
        popup.show();
    }
    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.it_one:
                Intent intent =new Intent(this, CustomerInformation.class);
                startActivity(intent);
                return true;
            case R.id.it_tow:
                Intent intent2 =new Intent(this, UserLocationActivity.class);
                startActivity(intent2);
                return true;
            case R.id.it_there:
                Toast.makeText(this, "item3", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }

    }//list of popup


    private List<CustomerModel> getDriverList() {
        return GsonUtil.getGson().fromJson(ReaderUtil.getResponse(R.raw.driver_users, this)
                , new TypeToken<List<CustomerModel>>() {
                }.getType());
    }


}