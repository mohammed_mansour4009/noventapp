package com.example.Driver.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Driver.R;
import com.example.Driver.Util.ResourcesUtil;
import com.example.Driver.model.CustomerModel;

import java.util.List;

public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.ViewHolder> {
    private List<CustomerModel> customerModelsList ;
    private Context context;


    public DriverAdapter(List<CustomerModel> customerModelsList) {
        this.customerModelsList = customerModelsList;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        context = recyclerView.getContext();
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {//inflate of layout and Components
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_more_information, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);//inflate of Components
        return viewHolder;
    }


    @Override
    public int getItemViewType(int position) {
        return R.layout.row_more_information;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {// put new data ever time


        holder.ivStatus.setImageResource(ResourcesUtil.getDrawableByName(
                customerModelsList.get(position).getIconStatus(), context));


        holder.tvNameCustomer.setText(customerModelsList.get(position).getName());
        holder.tvNumberCustomer.setText(customerModelsList.get(position).getNumber());
        holder.tvTimeOrder.setText(customerModelsList.get(position).getTime());
        holder.tvTimeReply.setText(customerModelsList.get(position).getResponse());
    }


    @Override
    public int getItemCount() {
        return customerModelsList.size();// here the issue
    }

    class ViewHolder extends RecyclerView.ViewHolder {//declare elements and but resources
        TextView tvNameCustomer;//fix name please tvNameCustomer
        TextView tvNumberCustomer;
        TextView tvTimeReply;
        TextView tvTimeOrder;
        ImageView ivStatus;
        ImageView ivMenu;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNameCustomer = itemView.findViewById(R.id.tv_name_customer);
            tvNumberCustomer = itemView.findViewById(R.id.tv_number_customer);
            tvTimeReply = itemView.findViewById(R.id.tv_time_reply);
            tvTimeOrder = itemView.findViewById(R.id.tv_history);
            ivStatus = itemView.findViewById(R.id.ic_status);
            ivMenu = itemView.findViewById(R.id.ic_menu);
        }


    }

}



