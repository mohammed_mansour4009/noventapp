package com.example.Driver.Util;

import android.content.Context;

import com.example.Driver.R;



public class ResourcesUtil {

    public static int getDrawableByName(String name, Context context) {
        return context.getResources().getIdentifier(
                String.valueOf(name),
                context.getString(R.string.drawable),
                context.getPackageName()
        );
    }
}
