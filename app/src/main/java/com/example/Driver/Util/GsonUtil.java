package com.example.Driver.Util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtil {
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    private static Gson gson = new GsonBuilder()//**
            .setDateFormat(DATE_FORMAT)
            .create();


    public static Gson getGson () {
        return gson;
    }
}