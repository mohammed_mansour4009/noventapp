package com.example.Driver.model;

public class CustomerModel {
    private String name;
    private String number;
    private String time;
    private String response;
    private String iconStatus;

    public String getIconStatus() {
        return iconStatus;
    }


    public void setIconStatus(String iconStatus) {
        this.iconStatus = iconStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}
