package com.example.Driver.model;

public class OrderInfoModel {
    private String icon;
    private String label;
    private String content;

    public OrderInfoModel(String icon,String label, String content) {
        this.icon = icon;
        this.label = label;
        this.content = content;
    }

    public String getIc() {
        return icon;
    }

    public void setIc(String icon) {
        this.icon = icon;
    }

    public String getTxUp() {
        return label;
    }

    public void setTxUp(String label) {
        this.label = label;
    }

    public String getTxDown() {
        return content;
    }

    public void setTxDown(String content) {
        this.content = content;
    }
}
